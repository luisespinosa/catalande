# README #

This repository contains data associated to the SEPLN 2014 paper[1] on Catalan Definition Extraction. The resource file contains the following datasets:

1) train.txt - Automatically extracted definitions and non-definitions from Viquipèdia

2) test.txt - Automatically extracted definitions and non-definitions from Viquipèdia, manually revised (non exhaustive)

3) definiendums.txt - Automatically extracted noun phrases in candidate definiendum position from all definitions in train.txt*

4) definiens.txt - Automatically extracted noun phrases in candidate definiens position from all definitions in train.txt*

5) terms.txt - Frequency distribution of all noun phrases found in definitions contained in train.txt*


Download link: https://drive.google.com/file/d/0B4dY7B_VR5juYVFNYzZEV1RXX28/view
*Unnormalized

-------------------------------------------------------------------------------------------------

[1] Espinosa-Anke, L., & Saggion, H. (2014). [Descripción y Evaluación de un Sistema de Extracción de Definiciones para el Catalán](https://rua.ua.es/dspace/bitstream/10045/40026/1/PLN_53_07.pdf). SEPLN.